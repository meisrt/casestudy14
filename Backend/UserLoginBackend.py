
from flask import Flask
app = Flask(__name__)
 
@app.route("/")
def hello():
    return "Welcome to Team 14's Database!"
 
if __name__ == "__main__":
    app.run(debug=True)