from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os
import hashlib

app = Flask(__name__)

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('index.js')
    else:
        return "Hello User!  <a href="/logout">Logout</a>"

#Error pages 
@app.errorhandler(404)
def page_error(e):
    return render_template("404.html")
@app.errorhandler(405)
def page_error(e):
    return render_template("405.html")

@app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == "POST":
        fname = request.form.get("first name")
        lname = request.form.get("last name")
        username = request.form.get("username")
        password = request.form.get("password")
        confirm = request.form.get("confirm")
        account_type = request.form.getlist('account_type')
        #MD5 hash 
        #hashed_pass = hashlib.md5(password.encode())

        #Went through: 
        if password == confirm:
            db.Execute("INSERT INTO Users(fname, lname, username, password)
                        VALUES(:fname,:lname,:username,:password)" ,
                        {"first name":fname, "last name":lname, "username":username, "password":hashed_password})
            db.commit()
            return redirect(url_for('login'))
        else:
            flash("Password Incorrect")
            return render_template("register.html")          
   return render_template("register.html") 
    
    
    #try:
     #   c, conn = connection()
      #  return("accepted")
   # except Exception as e:
    #    return(str(e))

@app.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == 'team14' and request.form['username'] == 'admin':
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return home()
def login():
    if request.method == "POST":
        username == request.form.get("name")
        password == request.form.get("password")

        userdata = db.execute("SELECT username FROM Users WHERE username:=username", {"username":username}).fetchone()
        passdata = db.execute("SELECT password FROM Users WHERE password:=password", {"password":password}).fetchone()

        if userdata is None: 
            flash("Username Not provided")
            return render_template("login.html")
        else: 
            flash("login success!")
            return render_template("success.html")
                
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

if __name__ == "__main__":
    app.secret_key = os.urandom(50)
    app.run(debug=True)